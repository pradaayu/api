import React, {Fragment, Component} from "react";
import {Link} from "react-router-dom";
import axios from "axios";
import MyButton from "../components/Button"

class HomePage extends Component {
    state = {
        countries: []
    }

    getByCountries = async() => {
        const res = await axios.get("https://api.covid19api.com/countries");
        this.setState({countries: res.data})
    }

    componentDidMount() {
        this.getByCountries() 
    }
    
    render() {
        const lists = this.state.countries.map(list =>
            <div key={list.IS02}>
                <p>{list.Country}</p>
                <Link to={`/${list.slug}`}>view detail</Link> 
            </div>
        )
        return(
            <Fragment>
                <h1>Covid-19 Report</h1>
                <button onClick={this.getByCountries}>click</button>
                <div style={countries}>
                    {lists}
                </div>
                <MyButton/>
            </Fragment>
        )
    }

}const countries = {
    display: "flex",
    flexDirection: "column"
}



export default HomePage;
