import React, {Fragment} from "react";
import HomePage from "../views/HomePage";
import CountryDetail from "../components/CountryDetail";
import {Route} from "react-router-dom";

const Routes = () => {
    return(
        <Fragment>
            <Route path="/" exact component={HomePage}/>
            <Route path="/:countries" exact component={CountryDetail}/>
        </Fragment>
    )
}

export default Routes;