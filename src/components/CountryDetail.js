//bikin component untuk nampung detail negara
//kalau di HomePage click negara, akan di-direct ke page ini
import React, {Fragment, useState} from "react";
import axios from "axios";
import {Link, useParams} from "react-router-dom";

const CountryDetail = () => {

    const[data, setData] = useState([])

    let {countries} = useParams/*parameter dari link*/() //cek di routes

    const getCountry = async() => {
        const res = await axios.get(`https://api.covid19api.com/country/south-africa/status/confirmed?from=2020-03-01T00:00:00Z&to=2020-04-01T00:00:00Z`)
        setData(res.data)
    }

    const lists = data.map(list =>
     <div>
         <p>confirmed: {list.Cases}</p>
     </div>
    )
    return(
        <Fragment>
            <h1>detail negara {countries}</h1>
            <button onClick={getCountry}>get</button>
            <div>
                {lists}
            </div>
            <Link to="/">back home</Link>
        </Fragment>
    )
}

export default CountryDetail;