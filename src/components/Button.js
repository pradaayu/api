import React from "react";
import {Button} from "antd";

const MyButton = () => {
    return(
        <Button type="primary">Ant Button</Button>
    )
}

export default MyButton;